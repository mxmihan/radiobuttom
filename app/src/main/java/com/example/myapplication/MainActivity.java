package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     FrameLayout view = findViewById(R.id.container);
       setContentView(R.layout.activity_main);
      //  getLayoutInflater().inflate(R.layout.activity_main, view, true);

        button = findViewById(R.id.textView);
        button.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {

        FiltrFragment fragment = new FiltrFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, FiltrFragment.TAG)
                .addToBackStack(null)
                .commit();
    }
}
