package com.example.myapplication.customview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;

import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.myapplication.R;

import java.util.ArrayList;


public class ExRadioButton extends RelativeLayout implements OnExRadioCheckable {
    private ImageView mImageView;
    private TextView mTextView;
    private RadioButton mRadioButton;
    // Constants
    public static final int DEFAULT_TEXT_COLOR = Color.TRANSPARENT;


    // Attribute Variables
    private String mText;
    private Drawable drawable;
    private int image;
    private int mTextColor;
    private int textSize;
    //in DP
    //Padding Image
    private int paddingImageRight;
    private int paddingImageLeft;
    private int paddingImageTop;
    private int paddingImageButtom;
    private int paddingImage;
    //in DP
// Padding Text
    private int paddingTextRight;
    private int paddingTextLeft;
    private int paddingTextTop;
    private int paddingTextButtom;
    private int paddingText;

    // Variables
    private Drawable mInitialBackgroundDrawable;
    private OnClickListener mOnClickListener;
    private OnTouchListener mOnTouchListener;
    private boolean mChecked;
    private ArrayList<OnCheckedChangeListener> mOnCheckedChangeListeners = new ArrayList<>();


    //================================================================================
    // Constructors
    //================================================================================

    public ExRadioButton(Context context) {
        super(context);
        setupView();
    }

    public ExRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(attrs);
        setupView();
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    public ExRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        parseAttributes(attrs);
        //   setAttribute(context, attrs);
        setupView();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ExRadioButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        parseAttributes(attrs);
        //setAttribute(context,attrs);
        setupView();
    }

    //================================================================================
    // Init & inflate methods
    //================================================================================

//    public  void   setAttribute  (Context context, @Nullable AttributeSet attrs) {
//
//
//        final TypedArray a = context.obtainStyledAttributes(
//                attrs, R.styleable.ExRadioButton, 0, 0);
//
//        if (mDebugViewAttributes) {
//            saveAttributeData(attrs, a);
//        }
//
//        Drawable background = null;
//
//        int leftPadding = -1;
//        int topPadding = -1;
//        int rightPadding = -1;
//        int bottomPadding = -1;
//        int startPadding = UNDEFINED_PADDING;
//        int endPadding = UNDEFINED_PADDING;
//
//        int padding = -1;
//        int paddingHorizontal = -1;
//
//
//        boolean startPaddingDefined = false;
//        boolean endPaddingDefined = false;
//        boolean leftPaddingDefined = false;
//        boolean rightPaddingDefined = false;
//
//
//        final int N = a.getIndexCount();
//        for (int i = 0; i < N; i++) {
//            int attr = a.getIndex(i);
//            switch (attr) {
//                case com.android.internal.R.styleable.View_background:
//                    background = a.getDrawable(attr);
//                    break;
//                case com.android.internal.R.styleable.View_padding:
//                    padding = a.getDimensionPixelSize(attr, -1);
//                    mUserPaddingLeftInitial = padding;
//                    mUserPaddingRightInitial = padding;
//                    leftPaddingDefined = true;
//                    rightPaddingDefined = true;
//                    break;
//                case com.android.internal.R.styleable.View_paddingHorizontal:
//                    paddingHorizontal = a.getDimensionPixelSize(attr, -1);
//                    mUserPaddingLeftInitial = paddingHorizontal;
//                    mUserPaddingRightInitial = paddingHorizontal;
//                    leftPaddingDefined = true;
//                    rightPaddingDefined = true;
//                    break;
//                case com.android.internal.R.styleable.View_paddingVertical:
//                    paddingVertical = a.getDimensionPixelSize(attr, -1);
//                    break;
//                case com.android.internal.R.styleable.View_paddingLeft:
//                    leftPadding = a.getDimensionPixelSize(attr, -1);
//                    mUserPaddingLeftInitial = leftPadding;
//                    leftPaddingDefined = true;
//                    break;
//                case com.android.internal.R.styleable.View_paddingTop:
//                    topPadding = a.getDimensionPixelSize(attr, -1);
//                    break;
//                case com.android.internal.R.styleable.View_paddingRight:
//                    rightPadding = a.getDimensionPixelSize(attr,
//
//
//
//
//
//
//
//
//
//
//


    private void parseAttributes(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs,
                R.styleable.ExRadioButton, 0, 0);
        Resources resources = getContext().getResources();
        try {
            //Text
            mText = typedArray.getString(R.styleable.ExRadioButton_text);
            mTextColor = typedArray.getColor(R.styleable.ExRadioButton_textColor, resources.getColor(R.color.colorPrimary));
            textSize = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_textSize, 12);

            paddingTextRight = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_textPaddingRight, -1);
            paddingTextLeft = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_textPaddingLeft, -1);
            paddingTextTop = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_textPaddingTop, -1);
            paddingTextButtom = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_textPaddingButtom, -1);
            paddingText = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_textPadding, -1);
            //Image
            image = typedArray.getResourceId(R.styleable.ExRadioButton_setImageRes, -1);
            drawable = typedArray.getDrawable(R.styleable.ExRadioButton_setDrawableRes);
            paddingImageRight = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_imagePaddingRight, -1);
            paddingImageLeft = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_imagePaddingLeft, -1);
            paddingImageTop = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_imagePaddingTop, -1);
            paddingImageButtom = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_imagePaddingButtom, -1);
            paddingImage = typedArray.getDimensionPixelSize(R.styleable.ExRadioButton_imagePadding, 0);
        } finally {
            typedArray.recycle();
        }
    }

    // Template method
    private void setupView() {
        inflateView();
        bindView();
        setCustomTouchListener();
    }

    protected void inflateView() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.custom_radio_button, this, true);
        mTextView = (TextView) findViewById(R.id.ex_text_view);
        mImageView = (ImageView) findViewById(R.id.ex_image_view);
        mRadioButton = (RadioButton) findViewById(R.id.ex_radio_button);
        mInitialBackgroundDrawable = getBackground();
    }

    protected void bindView() {

        if (mTextColor != DEFAULT_TEXT_COLOR) {
            mTextView.setTextColor(mTextColor);
        }
        if (image != -1) {
            mImageView.setBackgroundResource(image);
        }
        mImageView.setImageDrawable(drawable);
        RelativeLayout.LayoutParams paramsImage = (RelativeLayout.LayoutParams) mImageView.getLayoutParams();
        RelativeLayout.LayoutParams paramsText = (RelativeLayout.LayoutParams) mTextView.getLayoutParams();
        //в dp
        if (paddingImage > 0) {
            paddingImageRight = paddingImage;
            paddingImageLeft = paddingImage;
            paddingImageTop = paddingImage;
            paddingImageButtom = paddingImage;
        }

        paramsImage.setMargins(paddingImageLeft
                , paddingImageTop
                , paddingImageRight
                , paddingImageButtom);

        mImageView.setLayoutParams(paramsImage);

        if (paddingText > 0) {
            paddingTextRight = paddingText;
            paddingTextLeft = paddingText;
            paddingTextTop = paddingText;
            paddingTextButtom = paddingText;
        }
        mTextView.setText(mText);
        mTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

        paramsText.setMargins(paddingTextLeft
                , paddingTextTop
                , paddingTextRight
                , paddingTextButtom);
        mTextView.setLayoutParams(paramsText);
    }

    //================================================================================
    // Overriding default behavior
    //================================================================================

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        mOnClickListener = l;
    }

    protected void setCustomTouchListener() {
        super.setOnTouchListener(new TouchListener());
    }

    @Override
    public void setOnTouchListener(OnTouchListener onTouchListener) {
        mOnTouchListener = onTouchListener;
    }

    public OnTouchListener getOnTouchListener() {
        return mOnTouchListener;
    }

    private void onTouchDown(MotionEvent motionEvent) {
        setChecked(true);

    }

    private void onTouchSelect(MotionEvent motionEvent) {
        setChecked(true);
    }

    private void onTouchUp(MotionEvent motionEvent) {
       // setChecked(false);
        // Handle user defined click listeners
        if (mOnClickListener != null) {
            mOnClickListener.onClick(this);

        }
    }
    //================================================================================
    // Public methods
    //================================================================================

    public void setCheckedState() {
        setBackgroundResource(R.drawable.background_shape_preset_button__pressed);

        //   mRadioButton.setChecked(true);
        //    mTextView.setTextColor(mPressedTextColor);
        //    mImageView.setTextColor(mPressedTextColor);
    }

    public void setNormalState() {
        //  setBackgroundDrawable(mInitialBackgroundDrawable);

        setBackgroundResource(0);

        mRadioButton.setChecked(true);

    }

    public String getText() {
        return mText;
    }

    public void setText(String value) {
        mText = value;
    }
//
//    public Drawable getIcon() {
//        return drawable;
//    }
//
//
//    public void setIcon(Drawable value) {
//        drawable = value;
//    }


    //================================================================================
    // Checkable implementation
    //================================================================================

    @Override
    public void setChecked(boolean checked) {
        if (mChecked != checked) {
            mChecked = checked;
            if (!mOnCheckedChangeListeners.isEmpty()) {
                for (int i = 0; i < mOnCheckedChangeListeners.size(); i++) {
                    mOnCheckedChangeListeners.get(i).onCheckedChanged(this, mChecked);
                }
            }
            if (mChecked) {
                setCheckedState();
            } else {
                setNormalState();
            }
        }
    }


    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    @Override
    public void addOnCheckChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        mOnCheckedChangeListeners.add(onCheckedChangeListener);
    }

    @Override
    public void removeOnCheckChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        mOnCheckedChangeListeners.remove(onCheckedChangeListener);
    }

    //================================================================================
    // Inner classes
    //================================================================================
    private final class TouchListener implements OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    onTouchDown(event);
                    break;
                case MotionEvent.ACTION_UP:
                    onTouchUp(event);
                    break;
            }
            if (mOnTouchListener != null) {
                mOnTouchListener.onTouch(v, event);
            }
            return true;
        }
    }
}




